import math
from os import major

class ClassRoom:
    def __init__(self, addr, building, _floor, room):
        self.addr = addr
        self.buiding = building
        self._floor = _floor
        self.room = room
    def disPlayRoom(self): 
        print("Student room is at Building: ", 
        self.buiding, "Floor: ", self._floor, " Room Number: ", self.room,
         " Address: ", self.addr)
class Major: 
    def __init__(self):
        self.major_name = "Material Science"
    def displayMajor(self):
        print("Major is: ", self.major_name)

class StudentRoom(ClassRoom):
    def __init__(self, addr, building, _floor, room, student_name):
        super().__init__(addr, building, _floor, room) 
        self.student_name = student_name
        self.major_name = Major()

    def disPlayRoom(self): 
         print("Student ", self.student_name, "will study at: ", 
        self.buiding, "Floor: ", self._floor, " Room Number: ", self.room,
         " Address: ", self.addr)


# obj = StudentRoom("Phnom Penh, Wat Phnom", "Square Root", 5, "501", "Both Phanith")
# obj.disPlayRoom()

# obj.major_name.displayMajor()


# obj = ClassRoom("Phnom Penh, Wat Phnom", "Square Root", 5, "501")
# obj.disPlayRoom() 