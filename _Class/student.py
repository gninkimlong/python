class Room: 
    id = "001"
    name = "Prasat Lolei"
    size = 100
    description ="1 Floor, A building, A001"

# Firs method
print(Room.id) 
print(Room.name)

# Create Instance 
obj1 = Room()
obj2 = Room()
print(obj1.id, obj1.name, obj1.size, obj1.description)