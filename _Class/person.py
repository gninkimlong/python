class Person: 
    def __init__(self, first_name, last_name, sex, age, email, phone, dob):
        self.first_name = first_name
        self.last_name = last_name
        self.sex = sex 
        self.age = age 
        self.email = email
        self.phone = phone
        self.dob = dob 
    def update_sex(self, new_sex): 
        self.sex = new_sex
    def show_full_name(self): 
        print(self.first_name, " ",self.last_name)
    def show_person_detail(self):
        s = "" 
        if self.sex == "F": 
            s = "Miss."
        else: 
            s = "Mr."
        print(s, self.first_name," ",self.last_name, " Age: ", self.age, "Email: ", self.email, " Tel:", self.phone)
        print(self.first_name, " ", self.last_name, " Just a", self.person_status())
    
    def person_status(self): 
        status = ""
        if self.age >=0 and self.age< 5: 
            status = "Baby"
        elif self.age >=5 and self.age<15: 
            status = "Kid"
        elif self.age >= 15 and self.age< 20: 
            status = "teenager"
        elif self.age >= 20 and self.age < 30: 
            status = "Adult"
        else: 
            status = "Old"
        return status

obj = Person("Sok","Chan","M", 10, "sokchang168@gmail.com", "(855)12-123-456", "22/2/2022")
obj.show_full_name()
obj.show_person_detail()
# obj.person_status()

print("========================")
obj.first_name = "Kong"
obj.last_name = "Makara"
obj.age = 15
obj.show_full_name()
obj.show_person_detail()

print("========================")
obj1 = Person("Reach","Syden","M", 20, "syden168@gmail.com", "(855)12-123-456", "22/2/1993")
obj1.update_sex("F") #Change object's value by method
obj1.show_full_name()
obj1.show_person_detail()

print("========================")
_first_name = input("First Name: ")
_last_name = input("Last Name: ")
_sex = input("Sex: ")
_age = int(input("Input age: "))
_email = input("Input Email: ")
_tel  = input("input Telephone: ")
_dob = input("Input DOB: ")

obj2 = Person(_first_name, _last_name, _sex, _age, _email, _tel, _dob)
obj2.show_full_name()
obj2.show_person_detail()


