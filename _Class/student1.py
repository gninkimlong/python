class Room:
    def __init__(self, id, name, size, description, campus):
        self.id = id 
        self.name = name 
        self.size = size 
        self.description = description
        self.campus = campus

    def displayRoom(self):
        print("Room ID: ", self.id, " Room Name: ", self.name, \
            " Room Size: ", self.size, "Description : ", self.description, \
                "Campus: ", self.campus)

obj_room = Room("001", "Prasat Kob", 100, "First Floor A001", "New campus")
obj_room2 = Room("002", "Prasat Bayon", 99, "Second floor", "Old campus")

obj_room.displayRoom()
print("=================")
obj_room2.displayRoom()
