
# hello_str, containing the string Hello World.
hello_str = "Hello World"
hello_int = 21
hello_bool = True
hello_tuple = (21, 32)
hello_list = ["Hello", "this", "is", "a", "list"]

print("List", hello_list)
# This list now contains 5 strings. Notice that there are no spaces
hello_list = list() 
hello_list.append("Hello,") 
hello_list.append("this") 
hello_list.append("is") 
hello_list.append("a") 
hello_list.append("list")
print("Second List", hello_list)

# Dictionary 
hello_dict = { "irst_name" : "Liam", "last_name" :"Fraser",
"eye_colour" : "Blue" }
