string = 'GRET'
a, b, c, d = string # Same number on both sides
print(a, d)
a, b, c = string[0], string[1], string[2:] # Index and slice
print( a, b, c)
a, b, c = list(string[:2]) + [string[2:]] # Slice and concatenate
print( a, b, c) 
a, b = string[:2] # Same, but simpler
c = string[2:]
print( a, b, c)
(a, b), c = string[:2], string[2:] # Nested sequences
