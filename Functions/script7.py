def get_formatted_name(first_name, middle_name, last_name):
 """Return a full name, neatly formatted."""
 full_name = first_name + ' ' + middle_name + ' ' + last_name
 return full_name.title()

full_name = get_formatted_name('Reach', 'Sarey', 'Chan')
print(full_name)