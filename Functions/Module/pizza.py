def make_pizza(size, *toppings):
    print("\nMaking a " + str(size))
    for topping in toppings:
        print("- " + topping)