provinces = {
    "P": "Phnom Penh", "K": "Kandal", "S": "Svey Rieng", "SR": "Seam Riep"
}
# Sort dictionary 1
n_p = dict(sorted(provinces.items(), key=lambda item: item[1]))
print(n_p)
# Sort dictionary 2
provinces = {
    k: v for k, v in sorted(provinces.items(), key=lambda item: item[1])
}

print(provinces)
provinces["PP"]  = provinces.pop("P") 
provinces["PP"]  = "Phnom Penh World"
print(provinces)

print(provinces.keys())

print(provinces.items())

for k, v in provinces.items():
    print("Key is: ",k, "Value is: ", v)
