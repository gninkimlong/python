class Test:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
obj = Test("Daniel", "Kim", 18)
print(obj.first_name)

class Test1(Test): 
    def __init__(self, first_name, last_name, age, salary):
        super().__init__(first_name, last_name, age)
        self.salary = salary

obj1 = Test1(obj.first_name, obj.last_name, obj.age, 1200)
print(obj1.salary)