def copy_student(ls1, lst2): 
    while ls1:
        n = ls1.pop()
        lst2.append(n)
    
    return lst2

l_student1 = ["Seyha", "Wattana", "Sarey", "Mongkol", "Panith2", "Virak", "Vaneth"]
l_student2 = []
copy_student(l_student1[:], l_student2)

print("Show student in List1: ", l_student1)
print("Show student in List2: ", l_student2)