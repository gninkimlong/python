class Espresso:
	menu_type = "Drink"

print("Call by class name", Espresso.menu_type) # No need class instance 
obj = Espresso() # Instance object
print("Call by instance: ", obj.menu_type)

