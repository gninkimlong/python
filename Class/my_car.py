class Car: 
    def __init__(self, make, model, year):
        self.make = make 
        self.model = model
        self.year = year 
        self.odometer_reading = 0
    def get_descriptive_name(self): 
        """Return a neatly formatted descriptive name."""
        long_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return long_name.title()

    def update_odometer(self, mileage):
        """Set the odometer reading to the given value."""
        self.odometer_reading = mileage

    def read_odometer(self):
        """Print a statement showing the car's mileage."""
        print("This car has " + str(self.odometer_reading) + " miles on it.")

    def increment_odometer(self, miles):
        """Add the given amount to the odometer reading."""
        self.odometer_reading += miles

objCar = Car("Luxus", "SUV", 2021)
objCar.read_odometer()
objCar.odometer_reading =200
objCar.read_odometer()
print(objCar.get_descriptive_name())
objCar.update_odometer(1000)
objCar.read_odometer()

objCar.increment_odometer(10)
objCar.read_odometer()





