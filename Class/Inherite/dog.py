class Dog: 
    def __init__(self, name, type, weight):
        self.name = name 
        self.type = type
        self.weight = weight
    def displayDog(self): 
        print("Dog: ", self.name, " Type: ", 
        self.type, " Weight: ", self.weight)
class DogSell(Dog): 
    def __init__(self, name, type, weight):
        super().__init__(name, type, weight)
        self.price = 10
    def displayDog(self):
        print("Name: ", self.name, " Type: ", self.type, 
        "Weight: ", self.weight, " Price: ", self.price)
objSell = DogSell("Lucky", "Pitbull", 15)
objSell.displayDog()
