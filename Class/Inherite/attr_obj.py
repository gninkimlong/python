class Car: 
    def __init__(self, make, model, year):
        self.make = make 
        self.model = model
        self.year = year 
        self.odometer_reading = 0
    def get_descriptive_name(self):
        full_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return full_name.title()
    def read_odometer(self):
        print("This car has " + str(self.odometer_reading) + " miles on it.")
class Battery():
    """A simple attempt to model a battery for an electric car."""
    def __init__(self, battery_size=70):
        """Initialize the battery's attributes."""
        self.battery_size = battery_size
    def describe_battery(self):
        """Print a statement describing the battery size."""
        print("This car has a " + str(self.battery_size) + "-kWh battery.")
class ElectricCar(Car):
    """Represent aspects of a car, specific to electric vehicles."""
    def __init__(self, make, model, year):
        super().__init__(make, model, year)
        self.battery = Battery()
my_obj = ElectricCar('tesla', 'model SX', 2021)
print(my_obj.get_descriptive_name())
my_obj.battery.describe_battery()