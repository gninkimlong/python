apple = 42

def hello_world():
    print("I am inside hello_world")

def my_print():
    e_text = "Hello GreatX Center"
    print(e_text.title())


if __name__ == "__main__":
    print("Value of __name__ is: ", __name__)
    print("Going to call hello_world")
    hello_world()
    my_print()