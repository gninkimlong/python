list1 = [1, 2, 3]
list2 = ["Great", "Center", "Top"]

print(list2[0]) 

# Repeting list two time 
print(list2 * 2)

# Combine two lists to one list 
binding_list = list1 + list2
print(binding_list)

# Display list by start to end point
print(list1[0:2])