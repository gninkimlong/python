import pandas as pd  
import numpy as np 
r_num = np.random.randn(7, 5)
data = pd.DataFrame(r_num, index=('1', '2', '4','6', '0', '11', '15'), columns=(['a', 'b', 'c', 'd', 'e']))

print("===== Before reindex ========")
print(data)
new_data = data.reindex(['1', '2', '3', '4', '5', '6', '7'])

print("=================After Reindex =================")
print(new_data)

print("============ Check null and not null ===========")
chk_not_null = data.notnull()
chk_null = data.isnull()

# print(chk_not_null)

print(chk_null)
print("=============================")
data.fillna(0)

re_data = pd.DataFrame(data)
print(re_data)
