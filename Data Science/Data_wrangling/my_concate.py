import pandas as pd 
left_data = pd.DataFrame({
         'id':[1,2,3,4,5],
         'Name': ['Chan', 'Tola', 'Seyden', 'Bopha', 'Voleak'],
         'subject_id':['S1','S2','S4','S6','S5']})
right_data = pd.DataFrame({'id':[1, 2, 3, 4, 5],
         'Name': ['Senet', 'Mengly', 'Navuth', 'Neath', 'Channy'],
         'subject_id':['S2','S4','S3','S6','S5']})

data = pd.concat([left_data, right_data])
print(data)