from turtle import left
import pandas as pd 
import numpy as np 

left_data = pd.DataFrame({
         'id':[1,2,3,4,5],
         'Name': ['Chan', 'Tola', 'Seyden', 'Bopha', 'Voleak'],
         'subject_id':['S1','S2','S4','S6','S5']})
right_data = pd.DataFrame({'id':[1, 2, 3, 4, 5],
         'Name': ['Senet', 'Mengly', 'Navuth', 'Neath', 'Channy'],
         'subject_id':['S2','S4','S3','S6','S5']})
print(left_data)
print(right_data)
print("============ Merging ===========")
left_data.merge(right_data, left_on='Name', right_on='subject_id')
print(left_data)

