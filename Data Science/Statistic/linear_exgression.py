import seaborn as sb
import matplotlib.pyplot as plt
import pandas as pd 
# data = pd.read_json("https://newsapi.org/v2/everything?q=tesla&from=2022-02-24&sortBy=publishedAt&apiKey=a08d77da7eed461cb351e731142e6bdf")
# print(data)

# df = sb.load_dataset('tips')
# print(df)


# sb.regplot(x = "total_bill", y = "tip", data = df)
# plt.show()
_data = []
with pd.ExcelFile("/Users/kimlongngin/Desktop/Python-Class/Data Science/Statistic/Data Analysis.xlsx") as xls: 
    data_sheet2 = pd.read_excel(xls, 'Regression model')
    my_data = data_sheet2.loc[[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], \
        ["Weekly Wage$",  "Age left education"]]
    _data = pd.DataFrame(my_data)

sb.regplot(x = "Weekly Wage$", y = "Age left education", data = _data)
plt.show()

