from scipy.stats import poisson
import seaborn as sb
import matplotlib.pyplot as plt 

data_binom = poisson.rvs(mu=4, size=10000)
print(data_binom)
ax = sb.displot(data_binom,
                  kde=True,
                  color='green')
ax.set(xlabel='Poisson', ylabel='Frequency')
plt.show()