def student_list(*arr_student): 
    print(arr_student)

student_list("My student")
student_list("Student A", "Student B", "Student C", "Student D")
# Script(Interpreted language) Vs. Programming (C/C++, Java) => compile

def make_payment(price, *item): 
    print("Total is :", price, " of all items: ", item)

make_payment("Coca", "Chocolate", "Fanta", "Sprite", "Water", 1200)